# Plateformes en communs, website

## How to use

Shared config (used both in production and development) is in `_config.yml`.

The file `_config.dev.yml` is used only in dev, in addition to `_config.yml`, to override specific data.

Launching development server :

```bash
bundle exec jekyll serve --config _config.yml,_config.dev.yml
```
You should reach your website at [localhost:4000](localhost:4000)


## Specificities

- One config file per environnement (see above, "How to Use")
- All pages are in `_pages` folder. You may need to specify page permalink in page file frontmatter.

## Environments and branches

- `master` branch is pushed in `production`.
- `dev` branch is the most up to date branch. It is pushed in a `dev` (kind of staging) environnement. Staging url : http://dev--plateformes-en-communs.netlify.com


## Ressources

### Netlify

- [CMS](https://www.netlifycms.org/)
- [Netlify docs](https://www.netlify.com/docs/)

## Credits

This website is based on the [Andrew Banchich](https://github.com/andrewbanchich) jekyll version of the "Spectral" theme by [HTML5 UP](https://html5up.net/).


Original README from HTML5 UP:

```
Spectral by HTML5 UP
html5up.net | @ajlkn
Free for personal and commercial use under the CCA 3.0 license (html5up.net/license)


A big, modern, blocky affair with a mobile-style menu, fully responsive styling,
and an assortment of pre-styled elements. So, pretty much what you were expecting
-- except, of course, for how it's put together, specifically:

- It's built on Skel 3*, a leaner, more modular rewrite of my responsive framework.

  (* = still in development as of this writing)

- It uses flexbox*, which eliminates all kinds of terrible hacks and clunky layout
  stopgaps (like CSS grid systems).

  (* = not supported on IE8/9, but non-flexbox fallbacks are included)

- It uses Sass* a lot more intelligently, thanks in part to several new mixins
  and functions I've been working on (as well as a few by @HugoGiraudel).

  (* = still entirely optional if you prefer vanilla CSS :)

- A ton of other stuff.

In short, Spectral's the culmination of several new things I'm working on/trying out,
so please, let me know what you think :)

Demo images* courtesy of Unsplash, a radtastic collection of CC0 (public domain) images
you can use for pretty much whatever.

(* = not included)

AJ
aj@lkn.io | @ajlkn


Credits:

	Demo Images:
		Unsplash (unsplash.com)

	Icons:
		Font Awesome (fortawesome.github.com/Font-Awesome)

	Other:
		jQuery (jquery.com)
		html5shiv.js (@afarkas @jdalton @jon_neal @rem)
		background-size polyfill (github.com/louisremi)
		Misc. Sass functions (@HugoGiraudel)
		Respond.js (j.mp/respondjs)
		Skel (skel.io)

```

Repository [Jekyll logo](https://github.com/jekyll/brand) icon licensed under a [Creative Commons Attribution 4.0 International License](http://choosealicense.com/licenses/cc-by-4.0/).
