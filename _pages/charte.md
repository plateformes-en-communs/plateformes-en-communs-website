---
layout: page
title: Charte de Plateformes en Communs
---

Les membres de Plateformes en Communs sont des organisations ou collectifs structurant des relations de pair à pair entre individus et/ou organisations dans un objectif de production de services, de biens ou de connaissances et qui s’engagent à respecter les 5 principes de la charte suivante.

<b>1) Gouvernance inclusive</b><br>
Les plateformes sont des organisations ou des collectifs démocratiques dirigés par leurs membres.<br>Elles définissent la nature de leurs différentes parties prenantes (utilisateurs, travailleurs, propriétaires, collectivités locales...), leurs statuts et rôles respectifs dans la gouvernance. Elles tiennent ces informations compréhensibles et accessibles sur leur site et à jour.<br>
La gouvernance des plateformes recherche des pratiques inclusives pour ses parties prenantes, notamment pour ses utilisateurs et salariés.<br>
Les plateformes recherchent aussi souvent que possible la mise en place de mécanismes de décisions horizontales collectives et assure la transparence des décisions et des deliberations.

<b>2) Partage de la valeur</b><br>
Les plateformes peuvent être à but non lucratif, à lucrativité limitée ou lucratif mais doivent organiser une redistribution de la valeur générée vers l’ensemble des acteurs participant à la créer ou vers une mise en réserve impartageable considérée comme commun de la plateforme. Au delà, le cas échéant, la plateforme peut redistribuer vers la société civile ou vers des actions de solidarité.
<br>Les plateformes organisant directement ou indirectement des relations de travail ou de services s'engagent à protéger les travailleurs de la plateforme dans l'exercice de leur travail et à mettre en place une rémunération équitable. Les utilisateurs et les travailleurs de plateforme ont la possibilité de participer au capital de la plateforme en s'inscrivant dans le troisième principe coopératif tel que défini par l'Alliance Coopérative Internationale<a href="http://www.entreprises.coop/7-principes-cooperatifs/85-decouvrir-les-cooperatives/quest-ce-quune-cooperative/166.html" target="_blank" class="">*</a>.
<br>Une partie des réserves est impartageable et constitue le commun de la plateforme.

<b>3) Ethique des données</b><br>
Les plateformes sont transparentes sur l’utilisation des données individuelles. Elles recherchent le consentement des utilisateurs en cas d'utilisation externe de leurs données et informent sur les objectifs poursuivis et dans le cas de leur valorisation financière.
<br>Les plateformes sont transparentes sur l’utilisation de donnés pour déterminer la fixation des prix.<br>
Les données collectives des utilisateurs sont protégées. Leurs conditions et pays de stockage sont précisés. Les plateformes affichent les conditions dans lesquelles elles collectent, notent, diffusent et transfèrent les données.<br>
Les plateformes mettent en œuvre les moyens de compréhension de leurs contrats par les utilisateurs. Elles s’interdisent des changements unilatéraux de contrat sans notification préalable et informent les utilisateurs sur les enjeux.

<b>4) Production de Communs</b><br>
Les plateformes constituent des communs au service d'un projet et de l'ensemble de leurs utilisateurs et salariés. 
<br>Elles définissent également leurs propres ressources qui appartiennent à tous. Ces communs et les créations collectives réalisées sur la plateforme sont placés sous licence libre ou à réciprocité renforcée.

<b>5) Coopération entre les membres</b><br>
Les plateformes mettent en place des échanges de pratiques et d’outils en Communs permettant un co-développement et facilitant l’emergence de nouveaux projets.
<br>Les plateformes participent à la création d'un Commun des Communs en partageant au moins un élément de leur structure (statuts, logiciel, documentation du modèle économique...) avec divers degrés d'ouverture, de transparence et de partage, protégeant et étendant les Biens Communs.

<i>Charte en écriture collaborative permanente. Version du 19 novembre 2017</i>