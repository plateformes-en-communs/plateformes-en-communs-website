---
layout: page_for_html
title: "Qui sommes nous ?"
---

<section class="wrapper style5">
	<div class="inner">

		<h2> Présentation</h2>

		<p>Plateformes en Communs est un projet de la <a href="http://coopdescommuns.org/" target="_blank" title="lien vers la Coop des Communs">Coop des Communs</a>.
		Nous créons un écosystème français de plateformes collaboratives équitables, productrice de communs et prenant en compte les enjeux sociaux et sociétaux de leurs activités.
		</p>

		<p>Nous regroupons des personnes venant de cultures différentes (Economie Sociale et Solidaire, Économie collaborative, Communs) qui souhaitent créer des alliances et des convergences entre ces mondes.
		Nous pensons qu’une nouvelle génération de plateformes collaboratives émergent et nous accompagnons leur structuration et les innovations sociales qu’elles portent : nouvelles gouvernances plus inclusives pour leurs communautés, partage de la valeur, innovations organisationnelles...
		</p>

		<p>En créant des convergences entre économie sociale et solidaire, économie collaborative et
		communs, nous permettons à une nouvelle génération de plateformes collaborative d'émerger et
		de se développer en inventant de nouvelles intégrations de leurs communautés dans la gouvernance,
		le partage de la valeur et la production de communs.
		</p>

		<p>Nous permettons également à l'économie sociale et solidaire de se saisir pleinement des
		possibilités offertes par le numérique et les communs pour dépasser et accroître ses propres
		modèles de gouvernance et son modèle spécifique de développement.
		Les transformations permises par le numérique et les Communs sont en cours, Plateformes en
		Communs en regroupe les acteurs et facilite la création de Communs pour explorer par la
		recherche-action de nouvelles pratiques innovantes.
		</p>

		<ul class="actions"><center>
			<li><a href="{{ site.baseurl }}/charte" class="button button--full-height special">Notre charte de valeurs</a></li>
			<li><a href="{{ site.baseurl }}/manifeste" class="button button--full-height special">Notre manifeste</a></li></center>
		</ul>

	</div>
</section>


{% include section-objectives.html %}


<section class="wrapper style5">
	<div class="inner">

		<h2> L'équipe de Plateformes en Communs </h2>

		<h3>Les membres du Comité de pilotage :</h3>

		<dl class="people-list">
			{% for person in site.data.comite_pilotage %}
				<dt>{{ person.name }}</dt>
				<dd>{{ person.description }}</dd>
			{% endfor %}
		</dl>


		<h2> Sites sur les PlatformCoop et les Communs </h2>
		<p><li><a href="http://coopdescommuns.org/" target="_blank" class="">Coop des Communs</a></li>
		<li><a href="https://platform.coop" target="_blank" class="">Platform Coop (site international)</a></li>
		<li><a href="https://ioo.coop/directory/" target="_blank" class="">Recensement international des Platform Coop</a></li>
		<li><a href="http://commonstransition.org" target="_blank" class="">Transition vers les Communs</a></li>
		<li><a href="https://p2pfoundation.net" target="_blank" class="">P2P Foundation</a></li>
		<li><a href="http://ouishare.net" target="_blank" class="">Ouishare, collectif qui œuvre sur le sujet de la "société collaborative"</a></li>
		<li><a href="https://contributopia.org/fr/home/" target="_blank" class="">Contributopia, projet de Framasoft pour la création de Communs numériques</a></li>
		<li><a href="https://p2pfoundation.net" target="_blank" class="">P2P Foundation</a></li>
		<li><a href="https://scinfolex.com" target="_blank" class="">S.I.Lex : Blog pour décrypter et analyser les transformations du droit à l’heure du numérique</a></li>
		<li><a href="http://www.les-communs-dabord.org" target="_blank" class="">Les Communs d'abord : toute l'actualité sur les Communs</a></li></p>

		<h2> Crédits </h2>
		<ul class="copyright">
			<li>Réalisation technique: <a href="https://www.clairezuliani.com/" target="_blank">Claire Zuliani</a>.</li>
			<li>Design: <a href="https://html5up.net/spectral" target="_blank">HTML5 UP</a>.</li>
			<li>Theme Jekyll integration: <a href="http://andrewbanchi.ch" target="_blank">Andrew Banchich</a>.</li>
			<li>Hébergement <a href="https://www.netlify.com/" target="_blank">Netlify</a>.</li>
		</ul>
	</div>
</section>
