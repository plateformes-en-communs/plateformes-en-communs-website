---
layout: platform
title: 1Dlab
description: "Streaming culturel équitable"
image: 1dlab.jpg
---

## Qu'est-ce que 1Dlab ?
1D Lab s'éfforce à défendre ses deux missions posées il y a maintenant plus de trois ans.
Renforcer la diversité culturelle et mieux rémunérer les créateurs de cette diversité via un nouveau partage de la valeur.
<br>Pour se faire nous développons des solutions, des produits, des services numériques entre plateforme multimédia de streaming équitable 1D touch, un kiosque culturel numérique et une application mobile : DIVERCITIES qui éditorialise l'espace public et est véritablement un outil de communication innovant.

### Quels objectifs pour 1Dlab ?
Les nouveaux équilibres de la révolution numérique mettent en danger la diversité culturelle professionnelle et standardise inexorablement le propos artistique au risque de lisser la liberté d'expression.

### Quelles sont vos innovations en terme de gouvernances pour permettre à toute votre communauté de participer aux décisions ?
Grace à la SCIC SA nous avons pu réunir l'écosystème que nous souhaitons renforcer afin de réfléchir ensemble à de nouveaux équilibres.<br>
Créateurs, de contenus, Diffuseurs de contenus, Fédérations et Groupement, partenaires publics et privés et les salariés élisent un conseil d'administration, un président et un directeur général élu pour 3 ans.

### Quelles approches des communs pour 1Dlab ?
Nous abordons les communs sous l'angle des datas, de l'éditorialisation de ces datas culturelles. Nous appelons cela des "capsules culturelles" qui créent un véritable commun entre nos partenaires/clients afin qu'ils partagent leurs regards de prescription numérique de la matière créative que nous leur mettons à disposition.

### Quel est votre modèle économique ?
Notre modèle économique est en B to B to C
Une bibliothèque, un organisme de transport, une collectivité, un hôtel, prennent des abonnements à nos services pour mettre à disposition pour leur communauté ces services. C'est pour la pluspart du temps gratuit pour l'utilisateur final ou via un abonnement global à la bibliothèque par exemple.

### Que pouvez-vous mettre en commun pour aider d'autres plateformes ?
Témoigner sur la mise en place d'une SCIC SA, dans le monde numérique et mode start-up.
