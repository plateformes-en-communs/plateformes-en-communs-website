---
layout: platform
title: Fairbooking
description: "Réservation en direct d'hébergements touristiques"
image: Fairbooking.png
---

## Qu'est-ce que Fairbooking ?
Fairbooking.com est une plateforme de réservation en direct d'hébergements touristiques gérée par les hôteliers eux mêmes sous forme d'un association a but non lucratif.<br>
Plus de 3700 hébergements sont réservables au meilleur prix producteur dans plus de 62 Pays. Pour les voyageurs, la commission que les hébergeurs ne donnent pas aux géants du tourisme en ligne est redonnée sous forme d'avantage.<br>
Fairbooking est l'alternative durable, responsable et solidaire aux plateformes de type Booking, Google ou Expedia . Fairbooking c'est vous !

### Quels objectifs pour Fairbooking ?
Une réservation sur 2 d'hébergement est désormais effectuée sur des plateformes de type Booking.com, Expedia. Ces plateformes prélèvent de lourdes commissions qui fragilisent de part leur caractère monopolistique l'ensemble des acteurs du tourisme. C'est une énorme partie de l'argent du tourisme qui échappe aux pays producteurs via l'optimisation fiscale de ces plateformes. C'est l'emploi, la capacité des acteurs du tourisme à réinvestir sur les territoires qui est menacée.

### Quelles sont vos innovations en terme de gouvernances pour permettre à toute votre communauté de participer aux décisions ?
L'association Reservation en Direct (RED) est une association à but non lucratif qui milite pour informer le grand public, les médias et les institutionnels sur les enjeux du tourisme en ligne.<br>
RED gère la plateforme Fairbooking.com et le centre de formation RED ACADEMIE.<br>
Chaque hébergeur adhérent bénéficie des services de l'association, de la plateforme de réservation en direct Fairbooking.com et des services du centre de formation.<br>
L'adhésion donne droit à participer à l'Assemblée Générale et à voter les décisions.

### Quelles approches des communs pour Fairbooking ?
Que tous les acteurs du tourisme soient co-propriétaires de leur plateforme de réservation en direct et puissent participer à la gouvernance.<br>
En 2018, Fairbooking.com s'émancipe de son association mère en devenant une SCIC (Société Coopérative d’intérêt Collectif).

### Quel est votre modèle économique ?
Une cotisation annuelle de 150€ plus 2euros par chambre.<br>
ZERO commission sur les réservations.

### Que pouvez-vous mettre en commun pour aider d'autres plateformes ?
La nouvelle coopérative des acteurs du tourisme Fairbooking souhaite sous forme Coopérative proposer un écosystème digital de facilitation et interopérabilité au bénéfice des producteurs, pour l'expérience client et toujours pour défendre le circuit court de la réservation touristique en ligne.