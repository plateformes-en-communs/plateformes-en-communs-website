---
layout: platform
title: France Barter
description: "Réseau d'échanges BtoB"
image: francebarter.jpg
---

### Qu'est-ce que France Barter ?
La coopérative France Barter propose aux entreprises membres d'utiliser leurs actifs inexploités, stocks ou ressources disponibles comme mode de paiement grâce à la mise en place d'une monnaie interne à cette place de marché BtoB ; ainsi les entreprises membres peuvent acheter et vendre entre elles sur le réseau sans sortie de trésorerie.

Les équipes France Barter en région (Paris, Lyon, Savoie, Bretagne... et de nouvelles ouvertures à venir) accompagnent ces transactions non monétaires et permettent ainsi aux entreprises du réseau France Barter de se développer en trouvant facilement des clients nouveaux et des fournisseurs prêts à leur proposer des prestations ou marchandises de qualité facturées en Barter €uros !
D'abord sous la forme d'un réseau d'affaires online permettant à chaque membre de poster ses offres et ses besoins, l'échange inter-entreprises organisé par France Barter est un véritable outil de financement qui permet à un membre du réseau d'acheter une prestation ou des marchandises à crédit et de rembourser par des ventes réalisées sur le réseau.

### Quels objectifs pour France Barter ?
Une solution digitale pour permettre aux TPE et PME de mieux collaborer en limitant les contraintes budgétaires et favorisant des relations commerciales se basant sur de vraies synergies et sur de la confiance. Egalement d'avoir accès à un outil leur permettant des valoriser certains actifs inutilisés ou dormants qui peuvent interesser d'autres membres du réseau dans une logique d'économie circulaire. Enfin apporter une solution à un problème très français lié aux tensions de trésorerie des entreprises (projections de CA incertaines et delais de paiement) en apportant une capacité de financement alternative.

### Quelles sont vos innovations en terme de gouvernances pour permettre à toute votre communauté de participer aux décisions ?
Chaque entreprise qui veut rejoindre le réseau devient sociétaire de la SCIC France Barter et entre dès lors au multisociétariat : implication, confiance, transparence.
Les membres de la coopérative sont invités aux Assemblées générales, valident les comptes annuels et votent sur la stratégie à adopter pour développer le réseau.

### Quelles approches des communs pour France Barter ?
A ce jour la technologie France Barter est propriétaire (Vs open source) mais peut être "prêtée" à titre gracieux en certaines occasions.
Des expérimentations d'implementation de technologie blockchain pour décentraliser* au maximum la partie transactionnelle sont en cours.
<br>(*et s'affranchir de tiers de confiance au profit d'une implication de la communauté).

### Quel est votre modèle économique ?
235€ de frais d'adhésion.
5% de commission sur les transaction coté vendeur et coté acheteur.

### Que pouvez-vous mettre en commun pour aider d'autres plateformes ?
Des bonnes pratiques, des idées, de l'énergie et des salles à disposition sur Paris et Lyon (il s'agit de nos actifs dormants).