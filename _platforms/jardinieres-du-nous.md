---
layout: platform
title: "Jardinier.es du Nous"
description: "Co-construire des communautés apprenantes et des communs pérennes du faire ensemble"
image: jardinieres-du-nous.jpg
---

## Qu'est-ce que Jardinier.es du Nous ?

Jardinier.es du Nous est un projet collectif visant à créer et pérenniser des communautés apprenantes et de communs pédagogiques sur le faire ensemble et la gouvernance partagée.

Le projet est incubé par l'Université du Nous, association de loi 1901, et co-designé avec un premier cercle de partenaires de l'Economie Sociale et Solidaire, en cours de constitution. Il se place au service des acteur.ices de la transition démocratique et écologique.

L’expérience proposée est d’apprendre ensemble et en faisant. Le dispositif pédagogique comporte une plateforme numérique libre et open source et des groupes de pratique autonomes ou accompagnés.

### Quels objectifs pour Jardinier.es du Nous ?

Nous avons la conviction que pour permettre la transition démocratique à grande échelle, le changement se situe bien au-delà d’un changement de modèle. Il demande de revisiter en profondeur nos comportements compétitifs, notre rapport au pouvoir, pour passer du pouvoir SUR (l’autre, le groupe...) au pouvoir DE (de créer, de décider, d’initier ou de soutenir...). Les organisations et collectifs engagé.es dans la transition ont besoin que leurs parties prenantes montent en compétence ensemble sur la posture et la maîtrise des outils de gouvernance partagée.

Jardinier.es du Nous a pour objectif de fournir un environnement d'apprentissage à des groupes de pratique sur le faire ensemble, de réunir usagers et producteurs de ces nouvelles formes de gouvernance partagée qui contribuent à soutenir la transition démocratique et écologique.

Notre projet politique est d'offrir la possibilité à chaque citoyen·ne de cultiver au quotidien dans les collectifs et organisations auxquels il·elle participe, une nouvelle posture de coopération, et de contribuer ainsi à l’émergence d’une nouvelle culture plus coopérative.

### Quelles sont vos innovations en terme de gouvernances pour permettre à toute votre communauté de participer aux décisions ?

Jardinier.es du Nous est une organisation indépendante de l'Université du Nous et qui fonctionne en gouvernance partagée. Elle a initié en juin 2018 une démarche de co-design ouvert, dans laquelle les co-designers sont intégrés à la gouvernance de l'organisation.

L'intention est de structurer une SCIC avec l'ensemble des parties prenantes au projet.

Un enjeu fort d'innovation de cette plateforme est la mise en place d'une coopération inter-structures en réseau entre acteurs de la transition démocratique et écologique de différents domaines.

### Quelles approches des communs pour Jardinier.es du Nous ?

Nous nous inscrivons dans la vision de l'éducation et des connaissances comme biens communs pour l'humanité. Le projet vise à développer de façon pérenne des communs pédagogiques sur le faire ensemble et la gouvernance partagée.

Toutes les ressources pédagogiques présentes et futures du projet sont publiées sous licence ouverte. Les briques de développement nécessaires à la plateforme pédagogique ouverte sont libres, interopérables et open source.

### Quel est votre modèle économique ?

Le modèle économique est en cours de co-design avec la communauté. Il repose sur l’expérience de la Participation Consciente, qui a permis à l’Université du Nous de proposer ses séminaires et ses accompagnements à un large public pendant 8 ans.

Par ailleurs, nous avons entamé une réflexion de fond autour d’un système d’échange de temps entre acteurs de la transition, pour stimuler l’échange pairs à pairs entre nos réseaux.

### Que pouvez-vous mettre en commun pour aider d'autres plateformes ?

Nous pouvons mettre à disposition d'autres plateformes coopératives ouvertes des espaces d'apprentissage sur le faire ensemble et la gouvernance partagée, qui leur permettront de monter en compétence sur la gouvernance de leur projet avec toutes leurs parties prenantes.



