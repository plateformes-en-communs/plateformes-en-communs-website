---
layout: platform
title: Label Emmaüs
description: "La boutique en ligne avec vos valeurs"
image: labelemmaus.png
---

### Qu'est-ce que Label Emmaüs ?
Label Emmaüs a l’ambition de devenir un véritable contre-modèle aux sites marchands classiques, en portant sur le web la promesse d’Emmaüs : une deuxième vie pour les objets, une seconde chance pour les hommes.
<br>Contre-modèle de par sa gouvernance : Label Emmaüs est une coopérative, statut juridique à mi-chemin entre l’association et l’entreprise, qui concilie rentabilité économique et impact social. 
<br>Contre-modèle parce que l’activité est gérée de A à Z par les compagnons et les salariés en insertion, de la création des annonces à l’expédition, en passant par le suivi des commandes.
<br>Contre-modèle enfin, parce qu’au-delà de l’achat, Label Emmaüs ouvre de nouvelles formes de mobilisation citoyenne (coup de pouce solidaire à l’issue d’un achat, bénévolat de compétences..)

### Quels objectifs pour Label Emmaüs ?
Avec sa boutique en ligne, Emmaüs souhaite donner accès au plus grand nombre à ses bric-à-brac, renforcer sa présence sur la toile, et surtout poursuivre son action militante en faveur des personnes éloignées de l’emploi, en les formant aux métiers de demain. Plus de 50 compagnons et salariés en insertion ont été formés depuis le lancement du site il y a un an. L'objectif est de former au e-commerce plus de 300 personnes en situation d'exclusion d'ici 5 ans.

### Quelles sont vos innovations en terme de gouvernances pour permettre à toute votre communauté de participer aux décisions ?
Dans une économie où le terme « collaboratif » est de plus en plus galvaudé, Label Emmaüs a été créée sous le statut d’entreprise coopérative afin de faire vivre une participation citoyenne réelle et concrète. Ainsi, l’entreprise appartient à l’ensemble de ses parties prenantes : les acheteurs sont aux côtés du mouvement Emmaüs, des vendeurs et des salariés de la plateforme pour définir ensemble les orientations stratégiques en assemblée générale. Le poids d’un investisseur dans la coopérative ne dépend pas du montant qu’il aura investi : 1 homme = 1 voix. Ce que la coopérative valorise, c’est le geste militant avant tout. L’intégralité des bénéfices de l’entreprise est réinvestie dans l’outil de travail et les coopérateurs de Label Emmaüs sont unis autour d’une même mission : développer la formation et l’emploi des plus exclus sur les métiers en tension du e-commerce et du numérique et ainsi rendre effective la promesse d’un achat solidaire en ligne.

### Quelles approches des communs pour Label Emmaüs ?
Pour Emmaüs, la lutte pour les communs est historique et totale.<br>
De l'accès à l'eau en Afrique, à l'inclusion numérique, en passant par la libre circulation, Emmaüs est de tous les combats !

### Quel est votre modèle économique ?
Label Emmaüs est une marketplace, un tiers de confiance entre les vendeurs et les acheteurs.<br>
Son fonctionnement est financé par une commission sur les transactions passées sur la plateforme.<br>
Son modèle économique repose également sur une activité complémentaire de plateforme logistique lancée récemment en IDF (vente en ligne de livres invendus dans les centres Emmaüs et de mobilier professionnel collecté en entreprise). Label Emmaüs a reçu un agrément d'Entreprise d'Insertion pour cette nouvelle activité qui permettra d'embaucher 15 personnes en insertion d'ici 3 ans.

### Que pouvez-vous mettre en commun pour aider d'autres plateformes ?
Nous souhaitons mettre en commun  avec les autres plateformes notre expérience du statut coopératif et notre connaissance du monde associatif.<br>
Nous avons rencontré depuis notre création beaucoup de blocages administratifs, juridiques et financiers liés à notre statut et nous pensons qu'il est indispensable de s'associer à d'autres acteurs pour faire tomber ces barrières.