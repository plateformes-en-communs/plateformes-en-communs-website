---
layout: platform
title: Oiseaux de passage
description: "Hébergements, activités et bonnes adresses"
image: oiseaux.jpg
---

## Qu'est-ce que les Oiseaux de passage ?

La plateforme coopérative Les oiseaux de passage a été fondée pour pouvoir proposer l’accueil, la découverte de destinations, l’hospitalité et les échanges culturels d’humain à humain.<br>
C’est à la fois une Fabrique de Destinations - une nouvelle façon créative de coproduire des récits de territoire et des offres d’hospitalité et de découvertes grâce à des processus novateurs d’intelligence collective - et une Fabrique de Voyages -  une plateforme numérique collaborative qui facilite la création et le partage de voyages.

C'est une plateforme coopérative pour faire ensemble dans l’intérêt des communautés et des générations futures; la promesse d’un voyage qui soit source de rencontre, d’échange et de partage; l'accès à des offres qualifiées et enrichies par une logique de coopération; une plateforme libre, poétique et diffuse avec un modèle économique basé sur des tarifs plafonnés via un partage équitable des coûts.

### Quels objectifs pour les Oiseaux de passage ?
Les Oiseaux de passage a été lancé en 2014 à l’initiative de professionnels et citoyens afin d’inventer une plateforme coopérative sur la base des résultats encourageants de deux prototypes de plateformes d’hospitalité développées dans le cadre de capitales européennes de la culture en 2013 à Marseille et 2015 à Plzen.<br>
Ces prototypes, au croisement de l’économie collaborative, des droits culturels et des principes coopératifs, ont servis de support à deux années de travail collectif pour concevoir une plateforme innovante qui ne singe pas l’existant tout en répondant pleinement aux évolutions des usages et aux potentiels qu’offre internet. 
Les droits humains, les principes européens sur la valeur du patrimoine culturel, précisés dans la convention de Faro du Conseil de l’Europe et les principes coopératifs servent de cadre de référence.

Deux fils conducteurs ont nourri l’innovation : le choix du « commun » comme élément structurant et différenciant là où les plateformes actuelles misent sur l’individualisme et de retrouver le « plaisir de voyager » depuis la navigation sur la plateforme jusqu’au voyage lui même.
De là est née une plateforme coopérative poétique et diffuse, sans intrusion publicitaire ni commission, sans notation ni fichage, sans discrimination ni frontière et optimisation fiscale. Au contraire, une plateforme d’échange direct d’humain à humain, un accès à des offres d’hospitalité qualifiées réservables en ligne comme à des contenus culturels partagés librement et une navigation narrative comme cartographique pour retrouver le plaisir de voyager.

### Quelles sont vos innovations en terme de gouvernances pour permettre à toute votre communauté de participer aux décisions ?
Les oiseaux de passage est une société coopérative d'intérêt collectif dont les membres sont en majorité les communautés locales (celles qui proposent l'hospitalité et partagent leurs récits) auxquels s'ajoutent les réseaux nationaux (ceux qui fédèrent des producteurs  locaux), les prescripteurs (ceux qui relaient nos offres), les salariés et les soutiens (investisseurs, personnes ressources, etc). 

Les principes coopératifs sont un des cadres de références des Oiseaux de passage. Les communautés locales sont incitées à se réunir au sein d'une structure démocratique (asso, coop) pour devenir sociétaire de la SCIC. Les oiseaux de passage est une coopérative de second niveau. Les usagers (passagers, hôtes, etc) sont représentés via leurs fédération et/ou leur communauté locale.

### Quelles approches des communs pour les Oiseaux de passage ?
Le commun est l'un des deux fils rouge retenus pour inventer une plateforme coopérative qui ne singe pas celles existantes. La question du commun caractérise nos membres et traverse notre processus  : communautés locales, orienté vers le bien commun, prise en compte des biens communs, intérêts communs, mise en commun, etc.<br>
La question du commun s'incarne dans de nombreuses spécificités de la plateforme comme la mise en commun des récits, l'organisation basée sur les communautés locales, les filtres d'intérêt commun, etc.<br>

Plus particulièrement, le patrimoine culturel est abordé comme un "bien commun vécu" où le commun, la communauté et les biens communs sont indissociables. Les oiseaux de passage a comme cadre de référence la convention européenne sur la valeur du patrimoine culturel pour la société, dite Convention de Faro, et fait partie du "réseau de Faro" qui, sous l'égide du Conseil de l'Europe, ses membres échangent sur l’usage du patrimoine culturel comme source et ressource (bien commun vécu) des actions innovantes qu’ils mènent localement en réponse aux crises plurielles que nous connaissons : crise de la représentativité politique, crise du modèle économique et crise de la cohésion sociale face aux migrations.

### Quel est votre modèle économique ?
Les oiseaux de passage développe un réseau, une marque et une plateforme d’intermédiation web proposant l’hospitalité et la découverte de destinations par celles et ceux qui les font vivre.
<br>La marque commune Les oiseaux de passage© est utilisée par celles et ceux qui proposent l’hospitalité et la découverte : hébergeurs, guides, artistes, collectifs, fablab, etc. 

Ces producteurs locaux proposent et gèrent leurs propres productions en ligne : hébergements, activités, récits et recommandations.<br>
Ces offres et contenus sont accessibles selon les critères de situation géographique (géolocalisés),  prestation (tarifs, services, conditions), planification (disponibilités, programmations) et thématique et sont mis en récit collectivement par les producteurs sous quatre formes narratives : destination, itinéraire, récit et séjour.
Les producteurs sont cooptés par Les oiseaux de passage notamment à travers des conventions avec les fédérations partenaires autorisant leurs membres à ouvrir des destinations : 15 fédérations en 2018.

Économiquement, le modèle repose sur une logique de coopération et d’abonnement graduel aux services et commercialement par le relais des offres directement auprès des bénéficiaires des aides au départ.
Les oiseaux de passage développe des canaux privilégiés pour s’adresser directement aux passagers avec qui elle a des intérêts communs via les comités d’entreprises et mutuelles, la mutualisation entre producteurs, les médias en s’appuyant sur la force de ses contenus riches et multiples et l’agence de voyages Ekitour pour la diffusion auprès des comités d’Entreprises et des groupes.

### Que pouvez-vous mettre en commun pour aider d'autres plateformes ?
Les oiseaux de passage est partie prenante du mouvement international des plateformes coopératives et est en contact avec d'autres projets de plateformes coopératives comme FairBnB ou Fairbooking pour voir comment rendre interopérable nos plateformes coopératives dont les offres sont complémentaires. Il s'agit de faire vivre le 6me principes coopératif de "Coopération entre les coopératives : Pour apporter un meilleur service à leurs membres et renforcer le mouvement coopératif, les coopératives œuvrent ensemble au sein de structures locales, nationales, régionales et internationales."

### Un conseil à une plateforme qui souhaiterait se créer ?
Nous conseillons aux plateformes coopératives en cours de création de définir clairement leur coeur d'activité. La majorité des charges (développement, juridique, voire la marque) sont mutualisables avec d'autres coopératives. De plus les initiatives étant nombreuses, des retours d'expériences ou des aides ponctuelles sont tout à fait possibles entre coopératives.
