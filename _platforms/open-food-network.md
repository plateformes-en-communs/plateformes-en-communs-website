---
layout: platform
title: Open Food Network
description: "Circuits courts alimentaire de qualité"
image: OpenFood.png
---

## Quels objectifs pour Open Food Network ?

Les activités d’Open Food France s’organisent autour d’une même mission : soutenir le changement d’échelle des circuits courts en France. 

Nous pensons qu’une grosse partie des externalités négatives du système alimentaire (pollutions, maladies, perte de la biodiversité, déchets, suicides des paysans…) vient à la fois d’une grande centralisation du système alimentaire, contrôlé par quelques gros acteurs - en France 4 centrales  d’achat pèsent 92% du marché de l’alimentation (source) - et d’une grande déconnexion entre producteurs et mangeurs - dans les circuits classiques on sait rarement d’où viennent nos aliments et comment ils sont produits.

A travers le développement des circuits courts, nous soutenons des modèles d’organisation qui permettent de reconnecter producteurs et mangeurs, mais aussi, de décentraliser le système en soutenant l’émergence d’une myriade d’acteurs locaux aux modèles organisationnels et économiques variés : vente directe des producteurs, magasins de quartiers indépendants, groupements d’achat, AMAP, micro-marchés, etc. 

Par nos actions, nous souhaitons soutenir la réappropriation de leurs systèmes alimentaires par les individus, ceux qui produisent et mangent. C’est donc avant tout un enjeu de souveraineté alimentaire. Pour cela nous outillons les acteurs en leur mettant à disposition une plateforme en soutien à leur gestion commerciale (création de boutiques en ligne, administration des ventes). Nous travaillons également sur l’analyse, le partage et l’essaimage des différents modèles de circuits courts, et oeuvrons pour faciliter les coopérations entre acteurs, notamment en travaillant à l’interopérabilité des plateformes alimentaires.

## Comment aidez-vous ces circuits courts ?

De nombreux “hubs alimentaires” fonctionnent aujourd’hui avec des bouts de chandelles : certains groupements d’achat gèrent 1000 mails par semaines pour récupérer les achats de chacun des membres ! Open Food France apportent aux associations, collectifs, individus, entreprises qui organisent des circuits courts un outil pour leur permettre de gagner en efficacité et assurer la pérennité et le développement de leur projet.

Aujourd’hui, la plupart des outils en soutien aux hubs alimentaires soit imposent un modèle d’organisation particulier (ex: La Ruche qui Dit Oui), soit sont payants et à des niveaux de tarifs pas acceptables pour des initiatives parfois 100% bénévoles, soit manquent de flexibilité dans les fonctionnalités proposées pour répondre aux besoins spécifiques du hub, soit demandent des compétences techniques particulières.

Aussi, nous souhaitons faciliter et accompagner la création de nouveaux hubs alimentaires en accompagnant les porteurs de projets, via des guides d’accompagnement au choix du modèle de hub, via des programmes d’essaimages en lien avec les collectivités territoriales, etc. Ceci afin de soutenir la construction d’une offre en réponse à la demande aujourd’hui inassouvie de produits bios, locaux et accessibles.

## Quelles sont vos innovations en terme de gouvernances pour permettre à toute votre communauté de participer aux décisions ?

Dans l’esprit des plateformes coopératives, nous souhaitons que les utilisateurs de la plateforme soient aussi ceux qui décident de son évolution, et des projets portés pour soutenir le développement de l’écosystème.
Pour l’instant Open Food France est une association loi 1901, gérée par un bureau collégial et une Assemblée Générale permanente en ligne sur Loomio. Nous avons choisi le modèle associatif car notre mission première est d’intérêt général (développer les circuits courts).

Tout membre contributeur actif tel que désigné dans la Charte de l’association sur Loomio peut s’inscrire sur la liste des membres du collège. Concernant l’AG permanente, tout membre de la communauté peut à tout moment proposer une modification de fonctionnement de l’association, ou l’engagement de l’association dans un projet qui fait sens. Il est dans ce cas responsable de formuler une proposition sur Loomio, qui sera débattue et votée par la communauté. La prise de décision sur Loomio s’inspire de la sociocratie, en cas d’objection argumentée, le proposant peut amender sa proposition pour prendre en compte cette objection et formuler une nouvelle proposition.

Ce statut associatif est pour nous une étape temporaire. Nous avons prévu dans nos statuts la possibilité de nous transformer en SCIC lorsque le moment sera venu.

A noter : Open Food France s’inscrit dans le réseau international Open Food Network, qui compte des communautés dans une dizaine de pays. Nous avons également mis en place une gouvernance collaborative à  l’échelle internationale via une charte d’engagement mutuelle des différentes communautés locales et l’utilisation d’espaces de concertation (notamment le community forum) au quotidien.

## Quelles approches des communs pour Open Food ?

Notre approche des communs s’articule sur plusieurs dimensions:

Sur la plateforme Open Food France. Nous trouvons aberrant que chaque nouveau hub alimentaire doive redéployer son propre système, voir repayer pour un développement qui sera redondant à 80% avec les autres systèmes existants. Nous défendons la notion “d’infrastructures digitales”. Est-ce que dans le monde physique, chaque entreprise de transport doit construire sa propre route ? Non, la route est un commun et il y a des règles d’utilisation de ce commun. Les coûts de développement et de maintenance sont mutualisés. Pour nous, c’est pareil sur une infrastructure digitale. Les outils digitaux qui “structurent” l’organisation de notre société devraient être gérés comme des infrastructures. Le problème c’est que l’Etat n’ayant pas conscience de son rôle en terme de construction et maintien d’infrastructures digitales, et des entreprises privées, comme Google et son logiciel de cartographie Google Maps, qui développent ses infrastructures et par la même occasion, gagnent un pouvoir sur l’ensemble des activités qui utilisent cette infrastructure. Plutôt qu’une gestion publique, nous défendons l’approche des communs : une infrastructure digitale commune gérée par la communauté des usagers et autres parties prenantes. Bref, une plateforme coopérative.

Bien sûr, il va sans dire, le code de la plateforme est open source, sous licence AGPL 3. Cette licence oblige toute organisation qui ferait des développements sur le code source à partager ces développements selon la même licence. Nous partageons le même code source à l’échelle internationale, ce qui signifie que de nombreux développements peuvent être cofinancés.

Sur la connaissance, nous souhaitons aussi documenter et partager la connaissance relative aux modèles organisationnels, économiques, logistiques des différents hubs alimentaires, afin d’encapaciter là-encore un écosystème.

## Quel est votre modèle économique ?

L’association Open Food France a trois sources de financements : 
- **Les dons des utilisateurs du Communs** : les hubs qui utilisent la plateforme mais aussi les prestataires de services qui accompagnent des projets basés sur Open Food France. Open Food France ne réalise aucune prestation commerciale. Pour nous, “quand on utilise un commun, on doit contribuer au commun”, mais la nature et le montant de la contribution peuvent varier. Nous avons fait le choix aujourd’hui de laisser la liberté à chaque acteur de donner ce qui lui semble juste, et d’être transparent par rapport aux contributions de chacun.
- **Les subventions** : 2 Fondations nous soutiennent aujourd’hui, la Fondation MACIF et la Fondation Daniel & Nina Carasso
- **Les dons autres** : d’autres individus qui croient simplement à notre mission et nous font des dons réguliers ou ponctuels via HelloAsso


Au niveau international, chaque communauté locale contribue à hauteur de ses moyens au financement des rôles partagés (revue du code, curation de l’évolution produits, facilitation de la communauté globale, etc.). Nous utilisons pour cela l’outil de co-budgeting cobudget.

## Que pouvez-vous mettre en commun pour aider d'autres plateformes ?

Nous sommes ravis de partager avec d’autres plateforme notre modèle de gouvernance, notre modèle économique, et de réfléchir avec d’autres à la pérennisation de nos modèles économiques, ainsi qu’à la construction d’une voie politique, un “lobby des communs” pour faire comprendre à nos dirigeants l’efficacité sociale, économique et environnementale des communs.

## Un conseil à une plateforme qui souhaiterait se créer ?

Notre conseil a une plateforme qui voudrait se créer : le point clé est la construction et l’animation de la communauté, car pas de commun sans communauté ! Avec une communauté solide est engagée, on peut dépasser tous les obstacles :-)
