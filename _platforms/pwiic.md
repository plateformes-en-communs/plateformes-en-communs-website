---
layout: platform
title: Pwiic
description: "Echanges de services et d'objets entre particuliers"
image: pwiic2.jpg
---

## Pwiic ?

[Pwiic.com](https://pwiic.com) est une plateforme digitale européenne qui permet aux acteurs locaux d’échanger des services de pair-à-pair (entre particuliers), de consommer des services professionnels (faire appel aux professionnels locaux), de partager des frais (ex. covoiturage, potagers partagés), ou d’échanger, céder, mutualiser des objets : sur [Pwiic.com](https://pwiic.com), on trouve tout !

Le fonctionnement de Pwiic est basé sur AI (intelligence artificielle) : **PwiicBot**, un robot intelligent, est capable de comprendre une simple phrase, de la localiser, et de l’envoyer aux bonnes personnes, celles qui correspondent à la demande et se situent à proximité. 

Cependant, sur Pwiic.com, la technologie est au service de l'humain. Ainsi, on retrouvera au côté de PwiicBot, les **PwiicHums** : les humains de Pwiic. Les PwiicHums, sont les sociétaires de Pwiic. Ceux à qui la plateforme appartient. Car pwiic est une société coopérative !  Chaque membre PwiicHum peut prendre une part au capital de la startup pour 25 euros. Il existe des coopératives dans tous les secteurs d'activités. Bienvenue dans la version web des coopératives.

**Sur Pwiic.com, on partage tout, y compris la startup elle-même !**

La coopérative digitale Pwiic.com a été lancée en mars 2017. Elle comptait plus de 25.000 membres répartis en France et en Belgique, 6 mois après son lancement.

**Economie collaborative + véritable p2p + Intelligence artificielle (PwiicBot) + Humains (PwiicHums) + Coopérative = Pwiic**

## Pourquoi avoir créé Pwiic, en réponse à quelles problématiques ?

Pwiic.com est une alternative aux plateformes d'économie collaborative "on-demand" de type capitaliste, ex. taskRabbit.

**Constat** :  Beaucoup de projets d'économie collaborative partent d'une excellente intention. Mais, les startups sont souvent prises dans un engrenage.... Elles ont besoin de beaucoup de capitaux. Dans cette course aux capitaux, parfois, le projet est détourné de ses vraies valeurs. 

C'est pour cette raison que certaines plateformes dites d'économie collaborative, deviennent des plateformes de petits boulots (aussi nommées plateformes OnDemand), favorisant directement ou indirectement le statut précaire des "travailleurs collaboratifs" qui proposent leurs services sur le site..... 

Nous pensons avoir trouvé LA solution, pour que Pwiic.com ne soit JAMAIS détourné de ses valeurs .... :)

**Notre SOLUTION** : Il faut que les utilisateurs eux-mêmes participent au financement du site ... Mieux encore que ce soit eux qui le créent, qui dirigent l'entreprise.... Et miracle, ce modèle d'entreprise existe depuis 200 ans. Il s'agit du modèle COOPÉRATIF. :)

La mission de pwiic.com est de créer un réseau social (à l'image de Facebook ou Twitter), composé de communautés locales qui forment une communauté globale d’usagers (particuliers ou professionnels) et de permettre à ceux-ci de trouver ou de proposer des services et des objets en direct, sans intermédiaire, avec l’objectif d’un partage équitable du pouvoir et de la valeur (1), par le biais d'une plateforme digitale utilisant les technologies les plus innovantes (2).  


**Objectif (1) Partage du pouvoir et de la valeur.**

Pwiic.com est une association de personnes unies à travers une société commune gérée démocratiquement par ses sociétaires qui utilisent une plateforme digitale commune, le site Pwiic.com. Les plateformes coopératives sont une alternative aux plateformes digitales financées par le capital à risque nées dans le contexte de l'économie collaborative. 

Nous voulons apporter une réponse au besoin d’une économie collaborative plus responsable, plus respectueuse des valeurs de solidarité, d’équité, de transparence.  Nous sommes convaincus que la structure coopérative a le potentiel d'assurer la régulation financière et sociale de la plateforme grâce à la collaboration de ses membres. Notre mission est d'encourager une économie plus équitable et équilibrée en opposition aux modèles où la valeur générée est captée presque entièrement par des intermédiaires.  


**Objectif (2) La technologie au service de l’humain.**

La plateforme utilise des technologies avancées : intelligence artificielle (AI), Bot. Les mises en relation sont guidées par un robot intelligent (PwiicBot), capable de comprendre une phrase (200 caractères), de la localiser, et de l’envoyer aux bonnes personnes (acteurs locaux et citoyens, correspondant à la demande, et se situant à proximité de la demande). 

Pwiic.com est à ce jour la seule plateforme collaborative disposant d’un Bot intégré. Il s’agit d’un avantage majeur, sachant que dans les dix prochaines années, internet se développera dans ce sens. Le bot permet d’améliorer nettement l’expérience utilisateur, mais aussi d’alléger les coûts de la mise en relation. 

D’autres technologies seront mises en œuvre dans un avenir proche afin de gérer de manière efficace et objective le système de notation/réputation et les procédures de consultation/participation (voir ci-après).



## Nos Innovations en termes de gouvernance : Pwiic vise la Co-creation de l'entreprise avec des milliers d'internautes

Pwiic est une société coopérative de droit belge dont les statuts sont conformes aux critères d’agrément du Conseil National de la Coopération. Cependant, la volonté est d’aller au-delà des aspects purement formels de la gouvernance coopérative démocratique (participation à une assemblée générale statutaire). 

Pwiic a pour ambition :
- d'organiser une gouvernance qui s’appuie sur la participation active de ses sociétaires, 
- de susciter la co-responsabilisation dans le respect de la liberté et de la vie privée de chacun,
- de mettre en jeu le pouvoir de l'intelligence collective en prenant appui sur des relations interpersonnelles de qualité,
- de créer des relations interpersonnelles de pair à pair de qualité en créant la confiance mutuelle,
- de créer la confiance mutuelle en neutralisant l’intervention éventuelle d’un tiers « arbitre ».



Le défi est donc d'organiser cette gouvernance participative, responsabilisante dans la liberté et le respect de la vie privée et fondée sur la confiance. Pwiic vise la co-création de l'entreprise avec ses sociétaires. Pour cela il nous faut innover d'avantage et créer des nouvelles formes de gouvernances.

> Une gouvernance qui associe des milliers d'internautes entre-eux !  Quelle force de faire participer activement des milliers de personnes à la création d'un projet #commun ...



Pwiic s'organise par des milliers de micro-actions collectives de co-creation  !

Pwiic fonctionne avec une monnaie virtuelle locale = les crédits Pwiic. Pour utiliser Pwiic, il faut disposer de crédits Pwiic.

Chaque participation positive au développement de la startup (cocreation par des microactions collectives) permet de gagner des crédits Pwiic. Au plus on participe, au plus on gagne de crédits. Un tableau de bord avec les actions à réaliser est disponible dans chaque profil utilisateur. Il permet en temps réel de gagner des crédits Pwiic au prorata de sa participation (c'est la version moderne de ce qu'on appelle les "ristournes coopérateurs"). Chaque PwiicHum peut prendre en charge une ou plusieurs actions disponibles, ou en proposer de nouvelles à réaliser. Les micro-actions visent à améliorer le site, ou encore, à organiser la croissance.

Exemple de micro-action  : écrire un article pour le blog, répondre à une question d'un nouveau membre, faire connaître la plateforme en invitant des amis, en partageant des nouvelles sur les réseaux sociaux, en donnant des avis d'amélioration.



L'avantage des micro-actions collectives :

- **Stimuler l'implication online de nos PwiicHum au quotidien** : Comment impliquer des milliers d'internautes ? La plupart des sites, dépensent des dizaines de milliers d'euros en communication, pour impliquer les utilisateurs, qu'ils nomment souvent des ambassadeurs. En réalité, ces ambassadeurs, n'ont aucun droit...Et pourtant ce sont eux qui font en sorte que le projet se réalise. C'est leurs données, leurs contributions qui font exister des Facebook ou des Twitter. Mais comment cela se passerait-il si ces contributeurs étaient propriétaires de la plateforme ? Dans ce cas, pas besoin de dépenser des dizaines de milliers d'euros pour les convaincre, puisqu'ils sont impliqués légitimement, dès le départ....Et ça fonctionne à merveille pour Pwiic.com: des coûts marketing faibles, une implication forte de nos membres, un site efficace , ... 
- **Faciliter la communication**. Les micro-actions sont une première étape de sensibilisation et de communication. Grâce aux micro-actions, les PwiicHums auront très vite compris le fonctionnement de notre coopérative. C'est une manière simple d'expliquer notre modèle et nos différences par rapport à des plateformes capitalistes. Sachant que la majorité des internautes ne comprennent pas ce qu'est une coopérative, parfois ne savent pas du tout que ce modèle d'entreprise existe, l'enjeux "communication" est un enjeux de taille. 
- **Amener nos PwiicHum à participer aux décisions stratégiques et budgétaires**. C'est une responsabilité importante que de gérer une entreprise. Tout le monde ne souhaite pas forcement porter cette responsabilité. Si certaines PwiicHums préfèrent se limiter aux microactions, d'autres s'impliquent de plus en plus. C'est ainsi par exemple, que lorsque nous avons en septembre dernier décrochés 7000 euros via une campagne de crowdfunding, nos PwiicHums on pu choisir à quoi le budget serait alloué dans la liste  des améliorations définies collectivement. Les micro-actions sont un premier pas vers une implication plus conséquente.



## Notre approche des communs

Pwiic est une coopérative et respecte la Déclaration sur l’identité coopérative de l’Alliance Coopérative Internationale (1995). Cette Déclaration traite de la question du patrimoine commun dans la définition de la coopérative et dans le troisième principe coopératif :

La définition : *« Une coopérative est une association autonome de personnes volontairement réunies pour satisfaire leurs aspirations et besoins … au moyen d'une entreprise dont la propriété est collective … »*.

Le 3ème principe coopératif est intitulé « participation économique des membres » et stipule : "Les membres contribuent de manière équitable au capital de leurs coopératives et en ont le contrôle. Une partie au moins de ce capital est habituellement la propriété commune de la coopérative. Les membres ne bénéficient habituellement que d'une rémunération limitée du capital souscrit comme condition de leur adhésion. Les membres affectent les excédents à tout ou partie des objectifs suivants : le développement de leur coopérative, éventuellement par la dotation de réserves dont une partie au moins est impartageable, des ristournes aux membres en proportion de leurs transactions avec la coopérative et le soutien à d'autres activités approuvées par les membres ".


Ces règles sont en outre reprises, en Belgique, dans les critères d’agrément du Conseil National de la Coopération et Pwiic les a dès lors intégrées dans ses statuts. Il s’agit, techniquement, des règles concernant l’affectation des résultats et des fonds propres :


**Article 33. 2. Affectation des résultats**

33.2.1. Sur le résultat tel qu’il résulte des comptes annuels arrêtés par l’organe de gestion, il est prélevé au moins cinq pourcent (5 %) pour constituer la réserve légale ; ce prélèvement cesse d’être obligatoire lorsque le fonds de réserve atteint un dixième (1/10ème) de la part fixe du capital social. Il doit être repris lorsque la réserve légale vient à être entamée.

33.2.2. Après les prélèvements obligatoires et sur proposition de l’organe de gestion, l’assemblée générale détermine l’affectation du résultat. Toutefois aucune part sociale ne pourra se voir attribuer un dividende supérieur au taux maximum visé à l’article 1, § 2, 6° de l’Arrêté Royal du huit janvier mille neuf cent soixante-deux, fixant les conditions d’agréation des groupements nationaux de sociétés coopératives et des sociétés coopératives. 

33.3. Ristournes. La société peut accorder des bons d’achats de services, ristournes, avantages particuliers, points supplémentaires sur la plateforme au profit des Associés B, au prorata de leur participation à la vie de la plateforme, comme plus amplement détaillé dans le règlement d’ordre intérieur.

**Article 16. Droit au remboursement des associé**

… tout Associé B qui perd la qualité d’associé, pour l’une des causes énumérées à l’Article 12, a droit au remboursement de sa/ses part(s) à la seule concurrence de la valeur nominale de celle(s)-ci, à l’exclusion de toute participation aux réserves ou bénéfices quelconques, sous quelque forme que ce soit.

**Article 35. Liquidation**

35.2. Après apurement de toutes les dettes, charges et frais de liquidation ou consignation des sommes nécessaires à cet effet, le solde servira d’abord au remboursement du montant du capital libéré et ensuite, s’il reste encore une partie du solde, celui-ci sera affecté par décision prise par l’assemblée générale à des sociétés ou associations participant à l’économie sociale.

Notre approche des communs est structurée dans nos statuts par la création de deux parts sociales :
les parts A qui sont réservées aux coopératives ou autres institutions de l'ESS qui souhaitent contribuer et participer au projet (PwiicView)
les parts B qui sont réservés aux usagers (PwiicHum) 


Notre projet est un projet européen qui vise à terme a faire entrer au capital des acteurs situés dans toute l'Europe.
 


## Notre Modèle économique

Le modèle économique de Pwiic repose sur 3 hypothèses solidairement liées ; chacune est une condition nécessaire mais non suffisante :
- **Un chiffre d’affaire généré par notre monnaie locale (Les crédits Pwiics)** :
la plateforme ne prend pas de commissions sur les transactions financières réalisées entre les membres. Elle se rémunère d’abord par la vente de crédits Pwiics achetés par les utilisateurs. Pwiic fonctionne avec une monnaie virtuelle locale (les crédits Pwiics). Ces crédits sont dépensés sur la plateforme pour accéder à certaines opérations. Les sociétaires de Pwiic (= les PwiicHums)  actifs dans la gouvernance et dans certaines tâches gagnent des credits Pwiic en échange de leur participation et de leur actions de co-creation. Grâce à ces gains, les sociétaires bénéficient en tout ou en partie de la gratuité des services du site. Ceux qui n'ont pas le temps de participer pour gagner des crédits, peuvent en acheter. Notre chiffre d'affaire provient de l'achat de crédits Pwiics. 
- **Un volume d'activité très important (nombre d'échanges, utilisateurs,participation, etc)** :
l’économie collaborative est en forte croissance, mais aussi à forte concurrence. Pourtant, il n’y a pas de leader sur le marché. Dans ce type d’activité, le succès entraine le succès de manière exponentielle. C’est cette croissance exponentielle que nous voulons atteindre. Le volume est nécessaire puisque la volonté est de ne demander qu’une relativement faible contribution financière aux usagers de la plateforme. Pour que Pwiic soit rentable, la plateforme doit se composer de centaines de milliers d'utilisateurs. 
- **Des charges très faibles** :
 l’essentiel viendra des investissements et dépenses IT. Les frais dus à la gouvernance et l’administration du site doivent être très faibles, vu le concept qui consiste à permettre aux usagers de mettre en œuvre la gouvernance eux-mêmes et grâce à une technologie adéquate. De même, les frais de marketing doivent être très faibles. Le référencement doit résulter de l’activité des sociétaires PwiicHums qui peuvent prendre en charge, en échange de credits Pwiics, des actions simples et efficaces (microactions collectives) pour faire grandir la plateforme (écrire un article pour le blog, donner des avis, faire connaitre le projet sur les réseaux sociaux, inviter des amis, etc.).



## Ce que Pwiic souhaite mettre en commun avec d'autres plateformes

Des mises en commun sont envisageables et souhaitables dans deux domaines : sur le plan de la communication et sur le plan technologique.

Sur le plan de la communication, il faudrait organiser la mise en commun par des échanges croisés des capacités de communication et de référencement des différentes plateformes coopératives.

Sur le plan technique et technologique, beaucoup de mises en commun devraient être étudiées,
- Mise en commun de matériels informatiques (serveurs),
- Mise en commun de logiciels connexes et complémentaires au logiciel de base de la plateforme, tels que : les logiciels de gestion des systèmes de notation/réputation ou encore ceux de gestion des procédures de consultation/participation démocratique.
- Mise en commun de notre monnaie locale.
- Mise en commun de la coopérative Pwiic (via les parts A)


## Conseil à une plateforme qui souhaiterait se créer : être ambitieux !

Ne jamais se décourager, et garder la ligne stratégique et les objectifs fondamentaux qu’on a donné au projet et s’efforcer constamment de convaincre ceux qui, notamment dans les milieux de l’économie sociale et solidaire, éprouvent des craintes et se méfient de la digitalisation de l’économie. 

Pwiic est un projet hybride, qui met en avant la technologie (PwiicBot) et l'humain (PwiicHum). Cette technologie est utilisée pour alléger les frais de notre structure. Le caractère fortement technologique du projet, ne signifie pas pour autant que l'humain n'est pas au cœur de nos démarches. Nous souhaitons nous démarquer grâce à notre technologie innovante et grâce à notre gouvernance innovante. 

L'innovation technologique n'est pas réservée qu'aux startups de type capitalistes. Elle peut aussi être sociale et solidaire. Pourquoi l'économie sociale se contenterait-elle d'un marché de niche ? Pourquoi ne serions-nous pas ambitieux ? Le soutien de nos pairs est crucial. Notre conseil donc : appliquer le 6ème principe coopératif, favoriser l'inter-coopération, mettre nos efforts en commun.