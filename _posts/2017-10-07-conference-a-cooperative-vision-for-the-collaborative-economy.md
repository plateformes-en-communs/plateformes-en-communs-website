---
layout: post
title: Conférence "A cooperative vision for the collaborative economy" | 7 Nov | Bruxelles
description:
image: coopeurope.jpg
---

Organisé par Cooperatives Europe

Nicola Danti, Member of the European Parliament and EP Rapporteur on the Collaborative Economy, together with Cooperatives Europe will host the event titled "A cooperative vision for the collaborative economy" on 7th of November at the European Parliament.

🔸Across Europe, cooperatives have developed a patchwork of innovative models making the collaborative economy inclusive, community-led and people-centered, connecting technological innovation with social, economic and environmental needs.

🔸The event “A cooperative vision for the collaborative economy” will bring together European policy-makers, representatives of the cooperative movement, academics, leaders of cooperative enterprises and start-ups, to shape a European project on the future of the collaborative economy.

🔸Cooperatives Europe’s vision paper will be presented and released at this occasion.

Read the full programme [here](http://bit.ly/2wTfgnE) : [http://bit.ly/2wTfgnE](http://bit.ly/2wTfgnE)

REGISTER HERE [http://bit.ly/2hFay6D](http://bit.ly/2hFay6D)
Registrations close October 30, 12 PM

Meeting at 14:00 in front of the EP entrance for registrations
Altiero Spinelli, Place Luxembourg, European Parliament in Brussels
