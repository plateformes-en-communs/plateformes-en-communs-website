---
layout: post
title: Conférence du mouvement PlatformCoop international les 10 et 11 novembre 2017 à New-York
description: "THE PEOPLE’S DISRUPTION: PLATFORM CO-OPS FOR GLOBAL CHALLENGES"
image: platformcoop.jpg
---

REGISTER FOR $20 on [https://platform.coop/2017/register](https://platform.coop/2017/register)

The People’s Disruption: Platform Co-ops for Global Challenges
Experiments with cooperatively owned online platforms are demonstrating that democratic business models can be a dynamic force in building a more equitable economy for people across various income, race and class strata, starting with the most vulnerable populations.

The platform co-op movement disrupts Silicon Valley’s disruptors by shifting the focus toward fundamentally fairer forms of ownership and governance. The retirement of Baby Boomer business owners presents an opportunity for mass conversions of those businesses into co-ops. Existing cooperatives are increasingly eager to join the digital economy. Over the past few years, the burgeoning of platform co-ops, community currencies, worker’s tech, the solidarity economy, B-corps, and credit unions have shown us that alternative economies are not only necessary but possible.

Since the first platform cooperativism event at The New School two years ago, an ecosystem of people, knowledge, and tools has developed around this model. Now, some platform co-ops reverse-engineer the technologies of the “sharing economy” to create worker-owned rivals to Palo Alto’s most dominant tech firms. Others are developing enterprises of a kind the tech billionaires in California have not even considered.

To think and act our way out of the current crisis, we need to understand the roots of these extractive business models. With such insight, we will be able to build alternatives that best meet the needs of workers, consumers, and citizens.

The challenges that we are facing right now are hardly new; they have intensified over the past forty years. Managerial pay has increased prodigiously; income inequality has sharpened, affecting women and marginalized communities most acutely, and trillion of dollars of individual wealth have been tucked away in tax havens. Beyond that, there has been a general shift away from direct employment, leaving more and more workers vulnerable to stalled worker rights, as well as declining wages and benefits. At the same time, the Web has not made good on its cyber-utopian promises of democracy or social well-being; the rise of the digital commons and practices of peer production, while profoundly important, has not succeeded in generating business models that can deliver livelihoods for practitioners. With artificial intelligence on the rise, highly concentrated ownership of robots seems the likely outcome. The decentralized Internet has given way to an unprecedented centralization of data and platforms ownership.

Platform co-ops have emerged in areas like child care, art, journalism, transportation, social media, and food. Now, it is time to determine in which sector this business model works best and develop strategies for turning these experiments into robust answers to pressing challenges. How can public policy spur and protect cooperative platforms? What kinds of financing and legal support do they need?

The first platform cooperativism event in 2015 popularized the idea, and the second event in 2016 brought together co-op and union leaders to push the model forward. This third event will zero in on ways that platform cooperatives can help to address some of the world’s most urgent challenges. The fairer digital economy we need is already emerging everywhere around us.
