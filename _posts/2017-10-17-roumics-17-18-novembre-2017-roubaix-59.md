---
layout: post
title: ROUMICS les 17 et 18 Novembre 2017 à Roubaix (59)
description: Sur le thème «  Vivre des Communs »
image: roumics.jpg
event-date: 17 et 18 novembre 2017
---

En partenariat avec le collectif Catalyst et la Condition Publique, l’association ANIS organise en Novembre 2017 un nouvel événement ROUMICS, « Les Rencontres OUvertes du Multimédia et de l’Internet Citoyen et Solidaire » devenues incontournables au fil des ans en région Hauts de France pour tous les acteurs intéressés par le numérique et ses usages.
Cette 13ème édition des ROUMICS portera sur le thème «  Vivre des Communs » et aura lieu les 17 et 18 Novembre 2017 à la Condition Publique (Roubaix - 59).

Découvrez le programme : [http://www.roumics.com/IMG/pdf/Programme_Roumics_pages.pdf](http://www.roumics.com/IMG/pdf/Programme_Roumics_pages.pdf)<br>
S’inscrire : [https://www.helloasso.com/associations/anis/evenements/roumics-2017-vivre-des-communs](https://www.helloasso.com/associations/anis/evenements/roumics-2017-vivre-des-communs)
