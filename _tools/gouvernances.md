---
layout: tool
title: Gouvernances et Juridique
description : "Panel de statuts innovants permettant des démarches inclusives de la communauté dans la gouvernance et le partage de la valeur"
image: statuts.jpg
---

Lancement du groupe thématique le 27 février
