---
layout: tool
title: Logiciels, interopérabilités et intercoopérations
description: "Quelles interopérabilités entre plateformes pour permettre de l'intercoopération ? Quels logiciels pour innover dans l'inclusion de ma communauté dans la gouvernance de la plateforme ?"
image: logiciels.png
---

Lancement du groupe thématique le 27 février
