/*
	Spectral by HTML5 UP
	html5up.net | @ajlkn
	Free for personal and commercial use under the CCA 3.0 license (html5up.net/license)
*/

(function($) {

	skel
		.breakpoints({
			xlarge:	'(max-width: 1680px)',
			large:	'(max-width: 1280px)',
			medium:	'(max-width: 980px)',
			small:	'(max-width: 736px)',
			xsmall:	'(max-width: 480px)'
		});

	$(function() {

		var	$window = $(window),
			$body = $('body'),
			$wrapper = $('#page-wrapper'),
			$banner = $('#banner'),
			$header = $('#header');

		// Disable animations/transitions until the page has loaded.
			$body.addClass('is-loading');

			$window.on('load', function() {
				window.setTimeout(function() {
					$body.removeClass('is-loading');
				}, 100);
			});

		// Mobile?
			if (skel.vars.mobile)
				$body.addClass('is-mobile');
			else
				skel
					.on('-medium !medium', function() {
						$body.removeClass('is-mobile');
					})
					.on('+medium', function() {
						$body.addClass('is-mobile');
					});

		// Fix: Placeholder polyfill.
			$('form').placeholder();

		// Prioritize "important" elements on medium.
			skel.on('+medium -medium', function() {
				$.prioritize(
					'.important\\28 medium\\29',
					skel.breakpoint('medium').active
				);
			});

		// Scrolly.
			$('.scrolly')
				.scrolly({
					speed: 1500,
					offset: $header.outerHeight()
				});

		// Menu.
			$('#menu')
				.append('<a href="#menu" class="close"></a>')
				.appendTo($body)
				.panel({
					delay: 500,
					hideOnClick: true,
					hideOnSwipe: true,
					resetScroll: true,
					resetForms: true,
					side: 'right',
					target: $body,
					visibleClass: 'is-menu-visible'
				});

		// Header.
			if (skel.vars.IEVersion < 9)
				$header.removeClass('alt');

			if ($banner.length > 0
			&&	$header.hasClass('alt')) {

				$window.on('resize', function() { $window.trigger('scroll'); });

				$banner.scrollex({
					bottom:		$header.outerHeight() + 1,
					terminate:	function() { $header.removeClass('alt'); },
					enter:		function() { $header.addClass('alt'); },
					leave:		function() { $header.removeClass('alt'); }
				});

			}

	});
// Custom ===================================================
	// $("[data-is-form='ajax']").submit(function(e) {
	//   e.preventDefault();
	//   var $form = $(this);
	//   $.post($form.attr("action"), $form.serialize()).then(function() {
	// 		$("[data-is-result-form='ajax']").slideDown()
	//   });
	// });

})(jQuery);

// Contact form validation ------------------------
console.log("before Forms");

const contactForm = document.querySelector("[data-form-name='contact']");
if (!!contactForm) {
	console.log("contactForm");
	
	const message = contactForm.querySelector('textarea[name="message"]');
	const checkbox = contactForm.querySelector('input[name="privacy-rules-acceptance"]');
	const email = contactForm.querySelector('input[name="email"]');
	const phone = contactForm.querySelector('input[name="phone"]');

	contactForm.addEventListener("submit", function (event) {
		checkFieldValidity(message, event);
		checkFieldValidity(checkbox, event);
		checkMeansOfContactValidity(email, phone, event);
	}, false);
}

// Newsletter form validation ------------------------

const newsletterForm = document.querySelector("[data-form-name='newsletter']");
if (!!newsletterForm) {
	console.log("newsletterForm");
	
	const checkbox = newsletterForm.querySelector('input[name="privacy-rules-acceptance"]');
	const email = newsletterForm.querySelector('input[name="email"]');

	newsletterForm.addEventListener("submit", function (event) {
		event.preventDefault();

		let valid = checkFieldValidity(checkbox, event);
		// console.log(valid);
		if (valid) {
			let $form = $(newsletterForm);
			console.log($form);
			
			$.post($form.attr("action"), $form.serialize()).then(function () {
				$("[data-is-result-form='ajax']").slideDown()
			});
		}
		
	}, false);
}

// Form validation utilities ------------------------
function checkFieldValidity(field, event) {
	let fieldName = field.name;
	let error = document.querySelector(`[data-error='${fieldName}']`);
	
	if (!field.validity.valid) {
		addClass(field, 'with-error');
		addClass(error, 'active');
		event.preventDefault();
		return false
	}
	else {
		removeClass(field, 'with-error');
		removeClass(error, 'active');
		return true
	}
}

function checkMeansOfContactValidity(email, phone, event) {
	let emptyEmail = !email.value;
	let emptyPhone = !phone.value;
	let error = document.querySelector("[data-error='means-of-contact']");
	if (emptyEmail && emptyPhone) {
		addClass(email, 'with-error');
		addClass(phone, 'with-error');
		addClass(error, 'active');
		event.preventDefault();
	} else {
		removeClass(email, 'with-error');
		removeClass(phone, 'with-error');
		removeClass(error, 'active');
	}
}

function hasClass(el, className) {
	if (el.classList)
		return el.classList.contains(className)
	else
		return !!el.className.match(new RegExp('(\\s|^)' + className + '(\\s|$)'))
}

function addClass(el, className) {
	if (el.classList)
		el.classList.add(className)
	else if (!hasClass(el, className)) el.className += " " + className
}

function removeClass(el, className) {
	if (el.classList)
		el.classList.remove(className)
	else if (hasClass(el, className)) {
		var reg = new RegExp('(\\s|^)' + className + '(\\s|$)')
		el.className = el.className.replace(reg, ' ')
	}
}